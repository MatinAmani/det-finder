﻿using System;

class Program
{
	public static void Main()
	{
		Console.Write("Enter matrix degree: ");
		int degree = int.Parse(Console.ReadLine());
		double[,] A = ReadMatrix(degree);
		PrintMatrix(A, degree);
		double det = DetFinder(A, degree);
		Console.WriteLine($"det = {det}");
		Console.ReadKey();
	}

	public static double[,] ReadMatrix(int degree)
	{
		double[,] matrix = new double[degree, degree];
		for (int i = 0; i < degree; i++)
		{
			for (int j = 0; j < degree; j++)
			{
				Console.Write($"[{i + 1}, {j + 1}]: ");
				matrix[i, j] = double.Parse(Console.ReadLine());
			}
			Console.WriteLine();
		}
		return matrix;
	}

	public static void PrintMatrix(double[,] matrix, int degree)
	{
		for (int i = 0; i < degree; i++)
		{
			for (int j = 0; j < degree; j++)
			{
				Console.Write(matrix[i, j]);
			}
			Console.WriteLine();
		}
	}

	public static double[,] Minor(double[,] a, int i, int j, int degree)
	{
		//Minor Matrix Finder
		double[,] minor = new double[degree - 1, degree - 1];
		int iMin = 0, jMin;
		for (int k = 0; k < degree; k++)
		{
			jMin = 0;
			for (int l = 0; l < degree; l++)
			{
				if (k != i && l != j)
				{
					minor[iMin, jMin] = a[k, l];
					jMin++;
				}
			}
			if (k != i)
			{
				iMin++;
			}
		}
		return (minor);
	}

	public static double DetFinder(double[,] matrix, int n)
	{
		//Recursive Determinant Finder
		double det = 0;
		double[,] minor;
		if (n == 1)
		{
			det = matrix[0, 0];
			return (det);
		}
		else
		{
			for (int j = 0; j < n; j++)
			{
				minor = Minor(matrix, 0, j, n);
				det += Math.Pow(-1, j + 2) * matrix[0, j] * DetFinder(minor, n - 1);
			}
			return (det);
		}
	}
}